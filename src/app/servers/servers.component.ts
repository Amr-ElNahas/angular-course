import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  //styleUrls: ['./servers.component.css']
  styles: [`.whitefnt{
    color:white
  }`]
  
})
export class ServersComponent implements OnInit {
  allowNewServers = false;
  serverCreationStatus = 'no server was created'
  serverName = 'testServer'
  //username = ''
  serverCreated = false
  servers = ["TestServer", "TestServer2"]
  displayOn = true
  buttonClicks = [0]
  buttonClicksNum = 0
  constructor() {
    setTimeout(() => {
      this.allowNewServers = true;
    }, 2000)
  }

  ngOnInit(): void {
  }
  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName)
    this.serverCreationStatus = 'server was created! Name is ' + this.serverName
  }
  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value
  }
  onToggleParagaraph() {
    this.buttonClicksNum += 1
    this.buttonClicks.push(this.buttonClicksNum)
    console.log(this.buttonClicks)
    this.displayOn = !this.displayOn
    
  }
  //onServerReset() {
  //  this.username = '';
  //}
}
